// You should implement the following function
use std::time::{Duration, Instant};

fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0
    }
    if n == 1 {
        return 1
    }
    return fibonacci_number(n-1) + fibonacci_number(n-2)
}

fn fib2 (n: u32) -> u32 { // faster implementation
    if n == 0 {
        return 0
    }
    let mut values: [u32; 50] = [0; 50];  // currently only static allocated size
    values[1] = 1;
    for i in 2..(n+1) as usize {
        values[i] = values[i-1] + values[i-2]
    }
    values[n as usize]
}

fn main() {
    let num = 20;
    let now = Instant::now();
    println!("{}", fibonacci_number(num));
    let elapsed_time = now.elapsed();
    println!("Running fibonacci_number() took {} nanoseconds.", elapsed_time.as_nanos());

    let now2 = Instant::now();
    println!("Better algorithm is {}", fib2(num));
    let elapsed_time2 = now2.elapsed();
    println!("Running fib2() took {} nanoseconds.", elapsed_time2.as_nanos());
}
