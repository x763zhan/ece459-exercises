use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    for i in 0..N {
        let handle = thread::spawn(move|| {
            println!("Hi from thread {}!", i);
            thread::sleep(Duration::from_millis(1));
            i
        });
        children.push(handle);
    }
    while let Some(handle) = children.pop() {
        let num = handle.join().expect("Something wrong");
        println!("joined thread {}.", num);
    }
    // for i in 0..N {
    //     let handle = children.pop();
    //     match handle {
    //         Some(x) => {
    //             let num = x.join().expect("Something wrong");
    //             println!("joined thread {}.", num);
    //         },
    //         None    => println!("No more"),
    //     }
    //
    // }

}
// Spawning Threads:
//
// Create a program which spawns threads.
//
// This program should spawn threads until a user specified point.
// Use the .join function for the program to wait until every thread is outputed to end the program.
// Since threads run at the same time they will be outputted in a non specified order.