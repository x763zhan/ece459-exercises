
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 3;  // number of threads to spawn

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let mut children = vec![];

    for i in 0..N {
        let tx1 = mpsc::Sender::clone(&tx);
        let handle = thread::spawn(move||{
            let value = String::from(format!("what's up from {}", i));
            tx1.send(value).expect("Something is wrong with tx send");
            thread::sleep(Duration::from_secs(1));
            // if i == (N-1) {
            //     drop(tx1);
            // }
        });
        children.push(handle);
    }
    while let Some(handle) = children.pop(){
        handle.join().expect("Something wrong with thread");
    }
    let mut count = 0;
    for received in rx {
        println!("Got: {}", received);
        count+=1;
        if count == N {break;}
    }


    println!("Program finished");
}
//
// Sending and Receiving Threads Using Channels:
//
// Create a program which sends and receives strings using channels.
// mpsc::channel transmits a message while the receiver receives the message at the end of the program.
// There should be a 1 second time delay after each thread is transmitted.